const shipperRoleMiddleware = async (req, res, next) => {
  const {role} = req.user
  if (role !== 'SHIPPER') {throw Error('denied')}
  next()
}

module.exports = {shipperRoleMiddleware}
