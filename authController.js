const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')
const {userJoiSchema} = require('./User')
const {getNewGeneratedPassword, sendEmailWithNewPassword} = require('./authService')
const {saveUser, getUserByEmail, findUserByEmailAndUpdate} = require('./usersService')
const registerUser = async (req, res, next) => {const {email, password, role} = req.body
  await userJoiSchema.extract(['email']).validateAsync(email)
  await userJoiSchema.extract(['password']).validateAsync(password)
  await userJoiSchema.extract(['role']).validateAsync(role)
  await saveUser({email, password, role})
  return res.status(200).json({ message: 'user created' })
}
const loginUser = async (req, res, next) => {
  const {email, password} = req.body
  await userJoiSchema.extract(['email']).validateAsync(email)
  const user = await getUserByEmail(email)
  if(!user){throw Error(`no user`)}
  await userJoiSchema.extract(['password']).validateAsync(password)
  const isPasswordCorrect = await bcrypt.compare(String(password), String(user.password))
  if(!isPasswordCorrect){throw Error('wrong password')}
  const payload = {email: user.email, userID: user._id, role: user.role, created_date: user.created_date}
  const jwt_token = jwt.sign(payload, process.env.SECRET_JWT_KEY)
  return res.status(200).json({jwt_token, role: user.role})
}
const restorePassword = async(req, res, next) => {
  const {email} = req.body
  await userJoiSchema.extract(['email']).validateAsync(email)
  const user = await getUserByEmail(email)
  if(!user){throw Error(`no user`)}
  const newPassword = getNewGeneratedPassword()
  const newCryptedPassword = await bcrypt.hash(newPassword, 7)
  await findUserByEmailAndUpdate(email, {password: newCryptedPassword})
  await sendEmailWithNewPassword(email, newPassword)
  return res.status(200).json({message:'password sent to email'})
}

module.exports = {registerUser, loginUser, restorePassword}
