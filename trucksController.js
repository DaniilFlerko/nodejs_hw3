const {truckJoiShema} = require('./Truck')
const {getAllTrucksByUserID, addUserTruck, getUserTruckByID, updateUserTruckByID, deleteUserTruckByID,assignTruckToUserByID, getAssignedTruckByUserID, unassignUserTruckByID} = require('./trucksService')
const getAllTrucks = async(req, res) => {
  const userID = req.user._id
  const trucks = await getAllTrucksByUserID(userID)
  return res.status(200).json({trucks})
}
const saveTruck = async(req, res) => {
  const validatedData = await truckJoiShema.validateAsync({created_by: req.user._id,status: 'IS',...req.body})
  await addUserTruck(validatedData)
  return res.status(200).json({message: 'truck created'})
}
const getTruck = async(req, res) => {
  const userID = req.user._id
  const truckID = req.params.id
  const truck = await getUserTruckByID(userID, truckID)
  if(!truck){throw Error('no truck')}
  return res.status(200).json({truck})
}
const updateTruck = async(req, res) => {
  const truckID = req.params.id
  const type = req.body.type
  const payload = req.body.payload
  const {width, height, length} = req.body.dimensions
  await truckJoiShema.extract('type').validateAsync(type)
  await updateUserTruckByID(truckID, {type, payload, width, height, length})
  return res.status(200).json({message: 'truck changed'})
}
const deleteTruck = async(req, res) => {
  const truckID = req.params.id
  const truck = await deleteUserTruckByID(truckID)
  if(!truck){throw Error('no truck')}
  return res.status(200).json({message: 'truck deleted'})
}
const assignTruck = async(req, res) => {
  const userID = req.user._id
  const truckID = req.params.id
  const currentAssignedTruck = await getAssignedTruckByUserID(userID)
  if(currentAssignedTruck?.status === 'OL'){throw Error('active load')}
  if(currentAssignedTruck){throw Error('truck assigned already')}
  await assignTruckToUserByID(userID, truckID)
  return res.status(200).json({message: 'truck assigned'})
}
const unassignTruck = async(req, res) => {
  const userID = req.user._id
  const currentAssignedTruck = await getAssignedTruckByUserID(userID)
  if(!currentAssignedTruck){throw Error('no truck')}
  if(currentAssignedTruck.status === 'OL'){throw Error('load active already')}
  await unassignUserTruckByID(currentAssignedTruck._id)
  return res.status(200).json({message: 'truck unassigned'})
}

module.exports = {getAllTrucks, saveTruck, getTruck, updateTruck, deleteTruck, assignTruck, unassignTruck}
