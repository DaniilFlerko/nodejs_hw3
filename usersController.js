const bcrypt = require('bcryptjs')
const {User} = require('./User')
const fs = require('fs')
const {getUserByID, findUserByIDAndDelete, findUserByIDAndUpdate} = require('./usersService')
const getUserInfo = async(req, res, next) => {
  const {_id, email, role, created_date} = req.user
  return res.status(200).json({user: {_id, role, email, created_date: created_date}})
}
const deleteUser = async(req, res, next) => {
  const userID = req.user._id
  await findUserByIDAndDelete(userID)
  return res.status(200).json({message: 'user deleted'})
}
const updatePassword = async(req, res, next) => {
  let {_id, oldPassword, newPassword} = req.user
  const {password: currentPassword} = await getUserByID(_id)
  const isPasswordCorrect = await bcrypt.compare(String(oldPassword), String(currentPassword))
  if (!isPasswordCorrect) {throw Error('invalid password')}
  newPassword = await bcrypt.hash(newPassword, 7)
  await findUserByIDAndUpdate(_id, {password: newPassword})
  return res.status(200).json({message: 'password changed'})
}

module.exports = {getUserInfo, deleteUser, updatePassword}
