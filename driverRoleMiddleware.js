const driverRoleMiddleware = async(req, res, next) => {
  const {role} = req.user
  if(role !== 'DRIVER'){throw Error('no access')}
  next()
}

module.exports = {driverRoleMiddleware}
